export type HttpMethod = 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';

export type AuthCtx = {
  authHeader: string;
  method: HttpMethod;
  url: string;
  uri: string;
  host: string;
  pathname: string;
};

export type AuthUser = {
  userID: string;
  userRoles: string[];
};

export type AuthN = {
  authenticate(ar: AuthCtx): Promise<AuthUser | null>;
};

export type AuthZ = {
  authorize(ar: AuthCtx, au: AuthUser | null): Promise<boolean>;
};

export type JwksProvider = {
  getPublicKey(iss: string, kid: string): Promise<string | undefined>;
};

export type AuthConfig = {
  [host: string]: {
    [path: string]: {
      [method in HttpMethod]?: {
        authN: AuthN;
        authZ: AuthZ;
      };
    };
  };
};
