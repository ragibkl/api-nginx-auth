import Router from 'koa-router';

import { getAuth } from './getAuth';

export const authRouter = new Router();

authRouter.get('/', getAuth);
