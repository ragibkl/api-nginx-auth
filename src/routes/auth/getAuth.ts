import { RouterContext } from 'koa-router';

import { CognitoAuthN, SkipAuthN } from '../../services/authN';
import { RoleAuthZ, PublicAuthZ } from '../../services/authZ';
import { getAuthCtx, getAuthRoute } from '../../utils/request';

import { AuthConfig } from '../../types';

const cognitoAuthN = new CognitoAuthN([
  'https://cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_WzR1jrpga',
]);
const skipAuthN = new SkipAuthN();
const merchantAuthZ = new RoleAuthZ(['merchant', 'admin']);
const publicAuthZ = new PublicAuthZ();

export const AUTH_CONFIG: AuthConfig = {
  'dummy-api.apps2.bancuh.net': {
    '/events': {
      GET: { authN: cognitoAuthN, authZ: merchantAuthZ },
      POST: { authN: cognitoAuthN, authZ: merchantAuthZ },
    },
  },
  'requests-logs-auth.apps2.bancuh.net': {
    '/api/orders': {
      GET: { authN: cognitoAuthN, authZ: merchantAuthZ },
      POST: { authN: cognitoAuthN, authZ: merchantAuthZ },
    },
    '/api/orders/:id': {
      GET: { authN: cognitoAuthN, authZ: merchantAuthZ },
      POST: { authN: cognitoAuthN, authZ: merchantAuthZ },
    },
    '/api/catalog': {
      GET: { authN: cognitoAuthN, authZ: publicAuthZ },
    },
    '/api/login': {
      POST: { authN: skipAuthN, authZ: publicAuthZ },
    },
  },
};

export async function getAuth(ctx: RouterContext): Promise<void> {
  const authCtx = getAuthCtx(ctx);
  const authRoute = getAuthRoute(authCtx, AUTH_CONFIG);
  if (!authRoute) {
    ctx.status = 401;
    return;
  }

  const authUser = await authRoute.authN.authenticate(authCtx);
  if (authUser) {
    const { userID, userRoles } = authUser;
    ctx.set({
      'x-api-user-id': userID,
      'x-api-user-roles': userRoles.join(', '),
    });
  }

  const isAuthorized = await authRoute.authZ.authorize(authCtx, authUser);
  if (!isAuthorized) {
    ctx.status = 401;
    return;
  }

  ctx.status = 200;
}
