import Router from 'koa-router';

import { healthRouter } from './health';
import { authRouter } from './auth';

export const router = new Router();

router.use('/health', healthRouter.routes());
router.use('/auth', authRouter.routes());
