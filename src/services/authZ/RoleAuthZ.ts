import { intersection } from 'lodash';
import { AuthZ, AuthCtx, AuthUser } from '../../types';

export class RoleAuthZ implements AuthZ {
  roles: string[];

  constructor(roles: string[]) {
    this.roles = roles;
  }

  authorize(_ar: AuthCtx, authUser: AuthUser | null): Promise<boolean> {
    if (!authUser) {
      return Promise.resolve(false);
    }

    const overlapRoles = intersection(authUser.userRoles, this.roles);
    return Promise.resolve(overlapRoles.length > 0);
  }
}
