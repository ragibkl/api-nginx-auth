import { AuthZ } from '../../types';

export class PublicAuthZ implements AuthZ {
  authorize(): Promise<boolean> {
    return Promise.resolve(true);
  }
}
