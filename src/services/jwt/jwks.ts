import { JwksClient } from 'jwks-rsa';
import { JwksProvider } from 'types';

function createClient(jwksUri: string) {
  return new JwksClient({
    jwksUri,

    cache: true, // Default Value
    cacheMaxEntries: 5, // Default value
    cacheMaxAge: 600000, // Defaults to 10m

    rateLimit: true,
    jwksRequestsPerMinute: 10, // Default value
  });
}

export class Jwks implements JwksProvider {
  jwksClientMap: Record<string, JwksClient>;

  constructor(issuers: Array<{ issuer: string; jwksUri: string }>) {
    const clientMap: Record<string, JwksClient> = {};
    issuers.forEach(({ issuer, jwksUri }) => {
      clientMap[issuer] = createClient(jwksUri);
    });
    this.jwksClientMap = clientMap;
  }

  async getPublicKey(issuer: string, kid: string): Promise<string | undefined> {
    const client = this.jwksClientMap[issuer];
    if (!client) {
      return undefined;
    }

    const key = await client.getSigningKey(kid);
    return key.getPublicKey();
  }
}

export class CognitoJwks implements JwksProvider {
  jwksSource: Jwks;

  constructor(issuers: string[]) {
    const sources = issuers.map((issuer) => {
      const jwksUri = `${issuer}/.well-known/jwks.json`;
      return { issuer, jwksUri };
    });
    this.jwksSource = new Jwks(sources);
  }

  getPublicKey(issuer: string, kid: string): Promise<string | undefined> {
    return this.jwksSource.getPublicKey(issuer, kid);
  }
}
