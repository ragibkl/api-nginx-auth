import jwt from 'jsonwebtoken';

import { JwksProvider } from '../../types';

function getAuthToken(authHeader: string): string {
  if (authHeader.toLocaleLowerCase().startsWith('bearer ')) {
    return authHeader.slice('bearer '.length);
  }
  return authHeader;
}

export async function verify(authHeader: string, jwks: JwksProvider): Promise<jwt.Jwt | null> {
  const tokenString = getAuthToken(authHeader);

  const token = jwt.decode(tokenString, { complete: true });
  if (!token || !token.payload.iss || !token.header.kid) {
    return null;
  }

  const secret = await jwks.getPublicKey(token.payload.iss, token.header.kid);
  if (!secret) {
    return null;
  }

  try {
    const verifiedToken = jwt.verify(tokenString, secret, { complete: true });
    if (typeof verifiedToken === 'string') {
      return null;
    }
    return verifiedToken;
  } catch (error) {
    if (error instanceof jwt.JsonWebTokenError) {
      return null;
    }
    throw error;
  }
}
