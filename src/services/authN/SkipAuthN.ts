import { AuthN, AuthUser } from '../../types';

export class SkipAuthN implements AuthN {
  authenticate(): Promise<AuthUser | null> {
    return Promise.resolve(null);
  }
}
