import { isString } from 'lodash';
import { verify, CognitoJwks } from '../jwt';
import { AuthN, AuthCtx, AuthUser } from '../../types';
import { isStringArray } from '../../utils/isStringArray';

export class CognitoAuthN implements AuthN {
  jwks: CognitoJwks;

  constructor(issuers: string[]) {
    this.jwks = new CognitoJwks(issuers);
  }

  async authenticate(authRequest: AuthCtx): Promise<AuthUser | null> {
    const token = await verify(authRequest.authHeader, this.jwks);
    if (!token) {
      return null;
    }

    const userID = token.payload['cognito:username'];
    const userRoles = token.payload['cognito:groups'];

    if (!isString(userID) || !isStringArray(userRoles)) {
      return null;
    }

    return { userID, userRoles };
  }
}
