import { RouterContext } from 'koa-router';
import { find } from 'lodash';
import { match } from 'node-match-path';
import { AuthConfig, AuthCtx, HttpMethod } from '../types';

export function isHttpMethod(value: any): value is HttpMethod {
  if (['GET', 'POST', 'PUT', 'PATCH', 'DELETE'].includes(value)) {
    return true;
  }
  return false;
}

export function getAuthCtx(ctx: RouterContext): AuthCtx {
  const authHeader = ctx.get('authorization');
  const method = ctx.request.get('x-original-method');
  const uri = ctx.request.get('x-original-uri');
  const url = ctx.request.get('x-original-url');
  const { host, pathname } = new URL(url);

  if (!isHttpMethod(method)) {
    throw new Error('Invalid http method');
  }

  return {
    authHeader, // Bearer 1234
    host, // dummy-api.apps2.bancuh.net
    method, // 'POST'
    pathname, // /events
    uri, // '/events?abc=123'
    url, // 'http://dummy-api.apps2.bancuh.net/events?abc=123'
  };
}

export function getAuthRoute(authCtx: AuthCtx, authConfig: AuthConfig) {
  const authHost = authConfig[authCtx.host];
  if (!authHost) {
    return null;
  }

  const authRoute = find(authHost, (_r, path) => {
    const { matches } = match(path, authCtx.pathname);
    return matches;
  });
  if (!authRoute) {
    return null;
  }

  return authRoute[authCtx.method] || null;
}
